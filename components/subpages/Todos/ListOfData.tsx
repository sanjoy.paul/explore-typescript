import { useQuery } from '@tanstack/react-query';
import axios from 'axios';

type SheetProps = {
  id: number;
  name: string;
  title: string;
};

type GetSheetRes = {
  data: SheetProps[];
};

// const fetchSheetList = async () => {
//   const { data, status } = await axios.get<GetSheetRes>(
//     'https://sheetdb.io/api/v1/ol8u1ynrz60b3?sort_by=id&sort_order=desc'
//   );
//   console.log({ data });
//   return data || [];
// };

const fetchSheetList = async (): Promise<SheetProps[]> => {
  const res = await axios.get(
    'https://sheetdb.io/api/v1/ol8u1ynrz60b3?sort_by=id&sort_order=desc'
  );
  return res.data;
};

const ListOfData = () => {
  const { data, isLoading, isError, error } = useQuery<SheetProps[], Error>({
    queryKey: ['sheet'],
    queryFn: fetchSheetList,
  });

  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (error instanceof Error) {
    return <div>{error.message}</div>;
  }

  return (
    <div>
      <p>List of data</p>
      {(data as SheetProps[]).map((sheetDt: SheetProps) => (
        <div key={Math.random()}>
          <p>
            {sheetDt.name} {sheetDt.title}
          </p>
        </div>
      ))}
    </div>
  );
};

export default ListOfData;
