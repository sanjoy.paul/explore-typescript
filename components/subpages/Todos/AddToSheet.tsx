import { QueryClient, useMutation } from '@tanstack/react-query';
import axios from 'axios';
import { FormProvider, useForm } from 'react-hook-form';

interface IPerson {
  name: string;
  title: string;
}

interface ICreatePersonParams {
  id: string;
  name: string;
  title: string;
}

interface IContext {
  previousPerson: IPerson[] | undefined;
}

const AddToSheet = () => {
  const formMethods = useForm({
    defaultValues: {
      name: '',
      title: '',
    },
  });

  const queryClient = new QueryClient();

  const postTodos = (
    newTodo: ICreatePersonParams
  ): Promise<ICreatePersonParams[]> => {
    return axios.post('https://sheetdb.io/api/v1/ol8u1ynrz60b3', {
      data: [newTodo],
      return_values: true,
    });
  };

  const mutation = useMutation({
    mutationFn: postTodos,
    onMutate: async (newTodo: ICreatePersonParams) => {
      await queryClient.cancelQueries({
        queryKey: ['sheet'],
      });
      const prevTodos = queryClient.getQueryData(['sheet']);
      console.log({ prevTodos });

      if (prevTodos instanceof Array) {
        queryClient.setQueryData(['sheet'], [...prevTodos, newTodo]);
      }

      return { prevTodos };
    },
    onError: (err, newTodo, context) => {
      console.log({ context });

      if (context?.prevTodos) {
        queryClient.setQueryData(['sheet'], context.prevTodos);
      }
    },
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ['sheet'] });
    },
  });

  const onSubmit = formMethods.handleSubmit((values) => {
    console.log({ values });
    mutation.mutate({
      id: 'INCREMENT',
      name: values?.name,
      title: values?.title,
    });
  });
  return (
    <FormProvider {...formMethods}>
      <form onSubmit={onSubmit}>
        <input
          {...formMethods.register('name')}
          className="border"
          placeholder="enter name"
        />
        <input
          {...formMethods.register('title')}
          className="border"
          placeholder="enter title"
        />

        <button type="submit" className="border border-rose-500 px-4 py-1.5">
          Submit
        </button>
      </form>
    </FormProvider>
  );
};

export default AddToSheet;
