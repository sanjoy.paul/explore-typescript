import AddToSheet from './AddToSheet';
import ListOfData from './ListOfData';

const fetchTodos = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos');
  const result = await res.json();
  console.log('inside query', result);
  return result;
};

const Todos = () => {
  // const { data, isLoading, isError, error } = useQuery({
  //   queryKey: ['todos'],
  //   queryFn: fetchTodos,
  // });

  // if (isLoading) {
  //   return <div>Loading...</div>;
  // }
  // if (error instanceof Error) {
  //   return <div>{error.message}</div>;
  // }

  return (
    <div>
      <AddToSheet />
      <ListOfData />
    </div>
  );
};

export default Todos;
