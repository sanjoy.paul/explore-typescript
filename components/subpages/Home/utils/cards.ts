export const cards = [
  {
    id: 'card-1',
    title: 'Abstract figurative painting on canvas purple red blue color ',
    category: 'LenaKotliarker',
    ratings: 4.0,
    price: 15.5,
    imageURL: '/card.png',
    href: '#',
  },
  {
    id: 'card-2',
    title: 'Abstract figurative painting on canvas',
    category: 'LenaKotliarker',
    ratings: 2.0,
    price: 15.5,
    imageURL: '/card-1.png',
    href: '#',
  },
  {
    id: 'card-3',
    title:
      'Abstract figurative painting on canvas purple red blue color hello world hello world',
    category: 'LenaKotliarker',
    ratings: 3.0,
    price: 15.5,
    imageURL: '/card-2.png',
    href: '#',
  },
  {
    id: 'card-4',
    title:
      'Abstract figurative painting on canvas purple red blue color nice one',
    category: 'LenaKotliarker',
    ratings: 4.0,
    price: 15.5,
    imageURL: '/card.png',
    href: '#',
  },
  {
    id: 'card-5',
    title: 'Abstract figurative painting on canvas purple red blue color ',
    category: 'LenaKotliarker',
    ratings: 5.0,
    price: 15.5,
    imageURL: '/card-3.png',
    href: '#',
  },
  {
    id: 'card-6',
    title:
      'Abstract figurative painting on canvas purple red blue color how nice is this product',
    category: 'LenaKotliarker',
    ratings: 4.4,
    price: 15.5,
    imageURL: '/card-2.png',
    href: '#',
  },
];
