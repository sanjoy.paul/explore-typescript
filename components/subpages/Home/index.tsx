import { IHomePageProps } from 'pages';
import { useState } from 'react';
import AnimateButton from './AnimateButton';
import ReusableCard from './ReusableCard';
import { cards } from './utils/cards';

const Home = ({ data }: { data: IHomePageProps[] }) => {
  const [showTodos, setShowTodos] = useState(false);
  const [updateTodos, setUpdateTodos] = useState(data);

  return (
    <div className="container">
      {/* {showTodos &&
        updateTodos.slice(0, 5).map((dt) => <p key={dt.id}>{dt.title}</p>)}
      <button
        onClick={() => setShowTodos(!showTodos)}
        className="border border-rose-500 p-2"
      >
        Show todos
      </button>

      <AddTodos setUpdateTodos={setUpdateTodos} updateTodos={updateTodos} />

      <SlideShow /> */}

      <div className="grid grid-cols-1 gap-x-4 md:grid-cols-12">
        <div className="md:col-span-3">
          <h1>Hello Tourfam devs</h1>
        </div>
        <div className="md:col-span-9">
          <div className="grid gap-4 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:gap-5">
            {cards.map((card) => (
              <ReusableCard key={card.id} {...card} />
            ))}

            <AnimateButton />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Home;
