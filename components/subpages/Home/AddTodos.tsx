import { yupResolver } from '@hookform/resolvers/yup';
import { IHomePageProps } from 'pages';
import { Dispatch, SetStateAction } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import {
  IFormValues,
  initialValues,
  useFormHelpers,
  validationSchema,
} from './utils/formHelpers';

interface IAddTodosProps {
  setUpdateTodos: Dispatch<SetStateAction<IHomePageProps[]>>;
  updateTodos: IHomePageProps[];
}

const AddTodos = ({ setUpdateTodos, updateTodos }: IAddTodosProps) => {
  const formMethods = useForm<IFormValues>({
    defaultValues: initialValues,
    resolver: yupResolver(validationSchema),
  });

  const {
    formState: { errors },
  } = formMethods;

  const onSubmit = useFormHelpers(formMethods, setUpdateTodos);

  // const onSubmit = formMethods.handleSubmit((values: IFormValues) => {
  //   const todo = {
  //     userId: 1,
  //     id: Math.random(),
  //     title: values.firstName + ' ' + values.lastName,
  //     completed: values.completed,
  //   };
  //   if (values.completed) {
  //     setUpdateTodos([todo, ...updateTodos]);
  //     formMethods.reset();
  //   } else {
  //     alert('Please check');
  //   }
  // });

  return (
    <FormProvider {...formMethods}>
      <form onSubmit={onSubmit}>
        <input
          {...formMethods.register('firstName')}
          className="form-input rounded-full border px-4 py-3"
        />
        <span>{errors?.firstName?.message}</span>
        <input
          {...formMethods.register('lastName')}
          className="form-input rounded-full px-4 py-3 "
        />
        <span>{errors?.lastName?.message}</span>
        <input
          type="checkbox"
          {...formMethods.register('completed')}
          className="form-checkbox rounded text-pink-500"
        />

        <button type="submit" className="border border-rose-400 px-2 py-1">
          Submit
        </button>
      </form>
    </FormProvider>
  );
};

export default AddTodos;
