import { Dispatch, SetStateAction } from 'react';
import { FaBox, FaShoppingCart } from 'react-icons/fa';

interface IAddToCartButtonProps {
  isAdded: boolean;
  setIsAdded: Dispatch<SetStateAction<boolean>>;
}

const AddToCartButton = ({ isAdded, setIsAdded }: IAddToCartButtonProps) => {
  return (
    <button
      className="relative col-span-9 h-10 w-full overflow-hidden rounded-lg border-none bg-[#002E94] p-1.5 text-white outline-none transition duration-300 ease-in-out hover:bg-[#032674] active:scale-90 active:transform xl:col-span-8 2xl:h-11"
      onClick={() => setIsAdded(true)}
    >
      <span
        className={`text-white ${
          !isAdded ? 'opacity-1 animate-txt1' : 'opacity-1 animate-txt2'
        }`}
      >
        {isAdded ? 'Added' : 'Add to cart'}
      </span>
      <span
        className={`absolute top-1/2 -left-[10%] z-[2] -translate-x-1/2 -translate-y-1/2 transform text-3xl ${
          isAdded ? 'animate-cart' : ''
        }`}
      >
        <FaShoppingCart />
      </span>
      <span
        className={`font-2xl absolute -top-[20%] left-[52%] z-[3] -translate-x-1/2 -translate-y-1/2 transform ${
          isAdded ? 'animate-box' : ''
        }`}
      >
        <FaBox />
      </span>
    </button>
  );
};

export default AddToCartButton;
