import Image from 'next/image';
import Link from 'next/link';
import { CSSProperties, useState } from 'react';
import { FaStar } from 'react-icons/fa';
import AddToCartButton from './AddToCartButton';
import FavoriteButton from './FavoriteButton';

interface IReusableCardProps {
  title: string;
  category: string;
  price: number;
  ratings: number;
  imageURL: string;
  href: string;
}

const override: CSSProperties = {
  display: 'block',
  margin: '0 auto',
  borderColor: 'red',
};

const ReusableCard = ({
  title,
  category,
  price,
  ratings,
  imageURL,
  href,
}: IReusableCardProps) => {
  const [isAdded, setIsAdded] = useState(false);
  const [toggleButton, setToggleButton] = useState(false);

  return (
    <div className="w-full shadow-xl">
      <div className="relative flex h-full min-w-0 flex-col break-words rounded border bg-white bg-clip-border">
        <div className="relative h-[180px] sm:h-[160px] lg:h-[150px] xl:h-[180px] 2xl:h-[196px]">
          <Image
            src={imageURL}
            fill
            alt={title}
            sizes="100vw"
            className="h-full w-full rounded-tl-none rounded-tr-none"
            priority
          />
          {/* badge */}
          <div className="absolute top-5 right-0">
            <span className="bg-rose-500 py-1 px-2 text-xs font-medium uppercase text-white">
              Own product
            </span>
          </div>

          {/* favorite */}
          <FavoriteButton
            setToggleButton={setToggleButton}
            toggleButton={toggleButton}
          />
        </div>
        <span className="px-3 pt-4 text-sm text-[#002E94] underline underline-offset-4">
          {category}
        </span>
        <div className="flex-auto p-3">
          <Link href={href}>
            <h5 className={`block font-medium line-clamp-2 2xl:text-lg`}>
              {title}
            </h5>
          </Link>
        </div>
        <div className="p-3 pt-0">
          <div className="flex items-center space-x-1">
            {[...Array(5).keys()].map((star) => (
              <span
                key={star}
                className={`${
                  star < ratings ? 'text-[#002E94]' : 'text-gray-300'
                }`}
              >
                <FaStar />
              </span>
            ))}
            <span className="pl-1">({ratings.toFixed(1)})</span>
          </div>
          <div className="grid grid-cols-12 items-center justify-center pt-3">
            <h3 className="text-bold col-span-3 text-xl xl:col-span-4 xl:text-2xl 2xl:text-3xl">
              ${price}
            </h3>

            {/* cart button */}
            <AddToCartButton isAdded={isAdded} setIsAdded={setIsAdded} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReusableCard;
