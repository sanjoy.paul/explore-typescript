import { useRef } from 'react';
import { useClickAnimation } from './useClickAnimation';

const AnimateButton = () => {
  const buttonRef = useRef(null);
  useClickAnimation(buttonRef, {});

  return (
    <div>
      <button className="button-container" ref={buttonRef}>
        Click me
      </button>
    </div>
  );
};

export default AnimateButton;
