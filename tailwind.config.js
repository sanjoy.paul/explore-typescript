const keyframes = require('./config/keyframes');
const screens = require('./config/screens');
const spacing = require('./config/spacing');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    screens: {
      xs: '411px',
      sm: '540px',
      smd: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1400px',
      '3xl': '1920px',
    },
    extend: {
      ...keyframes,
      // keyframes: {
      //   cart: {
      //     '0%': { left: "-10%" },
      //     '40%, 60%': { left: "50%" },
      //     '100%': { left: "110%" },
      //   },
      //   box: {
      //     "0%, 40%": { top: "-20%" },
      //     "60%": { top: "40%", left: "52%" },
      //     "100%": { top: "40%", left: "112%" },
      //   },
      //   txt1: {
      //     "0%": {
      //       opacity: 1
      //     },
      //     "20%, 100%": {
      //       opacity: 0
      //     }
      //   },
      //   txt2: {
      //     "0%, 80%": {
      //       opacity: 0
      //     },
      //     "100%": {
      //       opacity: 1
      //     }
      //   },
      //   ...keyframes,
      // },
      // animation: {
      //   cart: 'cart 1s ease-in-out 1',
      //   box: 'box 1s ease-in-out 1',
      //   txt1: 'txt1 1s ease-in-out 1',
      //   txt2: 'txt2 1s ease-in-out 1',
      // }
    },
  },
  plugins: [
    require("@tailwindcss/forms")({
      strategy: 'base',
    }),
    function ({ addComponents }) {
      addComponents({
        '.container': {
          padding: '0px 1rem',
          '@screen xs': {
            maxWidth: 'auto',
            margin: '0px auto',
          },
          '@screen sm': {
            maxWidth: '462px',
            margin: '0px auto',
          },

          '@screen smd': {
            maxWidth: '462px',
            margin: '0px auto',
          },
          '@screen md': {
            maxWidth: '750px',
            margin: '0px auto',
            padding: '0px auto',
          },
          '@screen lg': {
            maxWidth: '970px',
            margin: '0px auto',
          },
          '@screen xl': {
            maxWidth: '1170px',
            margin: '0px auto',
          },

          '@screen 2xl': {
            maxWidth: '1250px',
            margin: '0px auto',
          },

          '@screen 3xl': {
            maxWidth: '1396px',
            margin: '0px auto',
          },
        },
        '.container-none': {
          padding: '0px 0px',
          maxWidth: '100%',
        },
      });
    },
    require('@tailwindcss/line-clamp'),
  ],
}
