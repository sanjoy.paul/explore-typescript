import Todos from '@/components/subpages/Todos';

const TodosPage = () => {
  return (
    <>
      <Todos />
    </>
  );
};

export default TodosPage;
